# TestSwift5

[![CI Status](https://img.shields.io/travis/gaoguohao/TestSwift5.svg?style=flat)](https://travis-ci.org/gaoguohao/TestSwift5)
[![Version](https://img.shields.io/cocoapods/v/TestSwift5.svg?style=flat)](https://cocoapods.org/pods/TestSwift5)
[![License](https://img.shields.io/cocoapods/l/TestSwift5.svg?style=flat)](https://cocoapods.org/pods/TestSwift5)
[![Platform](https://img.shields.io/cocoapods/p/TestSwift5.svg?style=flat)](https://cocoapods.org/pods/TestSwift5)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TestSwift5 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TestSwift5'
```

## Author

gaoguohao, gaoguohao@jiehun.com.cn

## License

TestSwift5 is available under the MIT license. See the LICENSE file for more info.
